import calendar
from datetime import datetime
from flask import redirect, session
from functools import wraps

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function

# extract time period from task
def process_timeline(task_data):
    timeline_data = []
    for task in task_data:
        assign = datetime.strptime(task['date_assigned'], '%Y-%m-%d %H:%M:%S')
        month = assign.month
        year = assign.year
        timeline = f'{month:02}-{year:4}'
        if timeline not in timeline_data:
            timeline_data.append(timeline)
    return timeline_data


# merege two relational data
def preprocess(user_data, task_data):
    # clone the user data
    dataset = user_data
    min_sprint = min([task['sprint'] for task in task_data])
    max_sprint = max([task['sprint'] for task in task_data]) + 1
    timeline_data = process_timeline(task_data)
    for user in dataset:
        # initialize monthly task points and time 
        user['performance'] = dict()
        for timeline in timeline_data:
            user['performance'][timeline] = {'total_points': 0, 'incomplete': 0, '1pt_task': 0, '2pt_task': 0, '3pt_task': 0, '1pt_time': 0, '2pt_time': 0, '3pt_time': 0}
        # intialize task based on sprint
        user['sprint'] = dict()
        for sprint in range(min_sprint, max_sprint):
            user['sprint'][sprint] = {'completed': 0, 'incomplete': 0}

    # import from task data to user
    for task in task_data:
        user = task['user_id'] - 1  
        point = task['points']
        sprint = task['sprint']
        assign = datetime.strptime(task['date_assigned'], '%Y-%m-%d %H:%M:%S')
        month = assign.month
        year = assign.year
        timeline = f'{month:02}-{year:4}'
        if task['status'] == 'Completed':
            complete = datetime.strptime(task['date_completed'], '%Y-%m-%d %H:%M:%S')
            time = complete - assign
            # based on pt_task
            dataset[user]['performance'][timeline][f'{point}pt_task'] += point
            dataset[user]['performance'][timeline][f'{point}pt_time'] = round(dataset[user]['performance'][timeline][f'{point}pt_time']\
                                                                      + time.seconds / 3600, 2)
            # based on sprint
            dataset[user]['sprint'][sprint]['completed'] += point
        else:
            dataset[user]['performance'][timeline]['incomplete'] += point
            dataset[user]['sprint'][sprint]['incomplete'] += point
        
    return dataset


# process the intergrated dataset on users
def process(dataset):
    n = len(dataset)
    for user in dataset:
        # total points completion monthly
        performance = user['performance']
        for timeline in performance:
            performance[timeline]['total_points'] = 1 * performance[timeline]['1pt_task'] \
                + 2 * performance[timeline]['2pt_task'] + 3 * performance[timeline]['3pt_task']
        
        # total experience points 
        user['total_points'] = sum([performance[timeline]['total_points'] for timeline in performance])
    
        # level of progression
        if user['total_points'] < 33:
            user['level'] = 'Fresher'
        elif user['total_points'] < 67:
            user['level'] = 'Junior'
        elif user['total_points'] < 100:
            user['level'] = 'Senior'
        else:
            user['level'] = 'Expert' 

        # average peer rating
        user_id = user['user_id']
        user['average_peer_rating'] = round((sum([peer['peer_rating'][user_id - 1] for peer in dataset]) \
                                    - user['peer_rating'][user_id - 1]) / (n - 1), 2)
        
        # completion rate
        points_completed = sum([user['sprint'][sprint]['completed'] for sprint in user['sprint']])
        points_assigned = points_completed + sum([user['sprint'][sprint]['incomplete'] for sprint in user['sprint']])
        user['completion_rate'] = round(points_completed / points_assigned, 4)

    # performance rank
    for user in dataset:
        user['rank'] = sorted(dataset, key=lambda x: x['total_points'], reverse=True).index(user) + 1


# whole ranking
def team_whole_rank(dataset, month_test):
    whole_rank = [{'name': f"{user['first_name']} {user['last_name']}", 'points': user['performance'][month_test]['total_points']} for user in dataset]
    whole_rank = sorted(whole_rank, key=lambda x: (x['points'], x['name']), reverse=True)
    return whole_rank[:8]


# team ranking
def team_rank(dataset, month_test):
    # process month
    this_month = int(month_test.split('-')[0])
    this_year = int(month_test.split('-')[1])
    if this_month > 1:
        last_month = this_month - 1
        last_year = this_year
    else:  
        last_month = 1
        last_year = this_year 
    last_month_test = f'{last_month:02}-{last_year}'

    # split dataset into teams
    team_set = set([user['team'] for user in dataset])
    teams_ones = dict()

    # monthly team rank
    for team in team_set:
        teams_ones[team] = [{'name': f"{user['first_name']} {user['last_name']}", 'last_points': user['performance'][last_month_test]['total_points'], 'points': user['performance'][month_test]['total_points']} for user in dataset if user['team'] == team]
        teams_ones[team] = sorted(teams_ones[team], key=lambda x: x['last_points'], reverse=True)
        for user in teams_ones[team]:
            user['last_rank'] = teams_ones[team].index(user) + 1
        teams_ones[team] = sorted(teams_ones[team], key=lambda x: (x['points'], x['name']), reverse=True)
        for user in teams_ones[team]:
            user['rank'] = teams_ones[team].index(user) + 1
            if user['last_rank'] > user['rank']:
                user['status'] = 'up'
            elif user['last_rank'] < user['rank']:
                user['status'] = 'down'
            else:
                user['status'] = 'same'

    return teams_ones


# team completion rate
def team_completion(task_data, dataset):
    team_set = set([user['team'] for user in dataset])
    teams_individuals = dict()
    teams_rate = dict()

    # team completion rate
    timeline_data = process_timeline(task_data)
    for team in team_set:
        teams_individuals[team] = [user for user in dataset if user['team'] == team]
        teams_rate[team] = dict()

        for timeline in timeline_data:
            # initialize the monthly completion rate
            teams_rate[team][timeline] = 0    
            # calculate the monthly completion rate            
            points_team_completed = sum([user['performance'][timeline]['total_points'] for user in teams_individuals[team]])
            points_team_assigned = points_team_completed + sum([user['performance'][timeline]['incomplete'] for user in teams_individuals[team]])
            teams_rate[team][timeline] = round(points_team_completed / points_team_assigned, 4)
    
    return teams_rate
