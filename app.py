from flask import Flask, jsonify, request, session
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_session import Session
import requests
import os
from dotenv import load_dotenv
from helpers import preprocess, process, team_whole_rank, team_rank, team_completion, login_required
from models import db, User
from config import ApplicationConfig

app = Flask(__name__)
CORS(app, supports_credentials=True) 

# configure session to use filesystem
app.config.from_object(ApplicationConfig)
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

# autoload env variables
load_dotenv()
WEBSITE = os.getenv('WEBSITE')
API_KEY = os.getenv('API_KEY')
# load the tasks API
task_response = requests.get(f'{WEBSITE}/tasks.json?key={API_KEY}')
task_data = task_response.json()
# load the users API
user_response = requests.get(f'{WEBSITE}/users.json?key={API_KEY}')
user_data = user_response.json()
# intergrate the data
dataset = preprocess(user_data, task_data)
# process the intergrated data for users' dashboard
process(dataset)
# analyze data for general dashboard
def whole_rank(month_test='06-2023'):
    return team_whole_rank(dataset, month_test)
def team_ranking(month_test='06-2023'):
    return team_rank(dataset, month_test)
team_rate = team_completion(task_data, dataset)

bcrypt = Bcrypt(app)
server_session = Session(app)
db.init_app(app)
with app.app_context():
    db.create_all()

# testing
# import random
# print([f"{user['user_id']}: {user['performance']['06-2023']['total_points']}, {user['performance']['06-2023']['incomplete']}" for user in dataset if user['team'] == 'Admin Portal'])
# print(teams['Admin Portal']['monthly_completion_rate'])
# print(whole_rank)
# print([f"{team}: {team_completion[team]}" for team in team_completion])
# print([(user['total_points'], user['level']) for user in dataset])

def initial_register():
    for user in dataset:
        user_exists = User.query.filter_by(username=user['user_name']).first() is not None
        if user_exists:
            return jsonify({'message': 'User already registered'}), 409
        hashed_password = bcrypt.generate_password_hash(user['password'])
        max_user_num = db.session.query(db.func.max(User.user_num)).scalar() or 0
        new_user_num = max_user_num + 1
        db.session.add(User(username=user['user_name'], password=hashed_password, user_num=new_user_num))
    db.session.commit()
    return jsonify({'message': 'All users registered successfully'})

with app.app_context():
    initial_register()

@app.route('/register', methods=['POST'])
def register_user():
    session.clear()
    username = request.json['username']
    password = request.json['password']
    cf_password = request.json['cf_password']
    user_exists = User.query.filter_by(username=username).first() is not None
    if user_exists or password != cf_password:
        return jsonify({'message': 'User already registered'}), 409
    hashed_password = bcrypt.generate_password_hash(password)
    max_user_num = db.session.query(db.func.max(User.user_num)).scalar() or 0
    new_user_num = max_user_num + 1
    new_user = User(username=username, password=hashed_password, user_num=new_user_num)
    db.session.add(new_user)
    db.session.commit()
    session['user_id'] = new_user.id
    return jsonify({'id': new_user.id, 'username': new_user.username})


@app.route('/@me')
@login_required
def get_user():
    user_id = session.get('user_id')
    if not user_id:
        return jsonify({'message': 'Unauthorized'}), 401
    user = User.query.filter_by(id=user_id).first()
    try:
        user_api = dataset[int(user.user_num) - 1]
        name = user_api['first_name'] + ' ' + user_api['last_name']
        team = user_api['team']
    except:
        name = 'New User'
        team = 'Not Applicable'
    return jsonify({'id': user.user_num, 'username': user.username, 'name': name, 'team': team})


@app.route('/login', methods=['POST'])
def login_user():
    session.clear()
    username = request.json['username']
    password = request.json['password']
    user = User.query.filter_by(username=username).first()
    if user is None or not bcrypt.check_password_hash(user.password, password):
        return jsonify({'message': 'Unauthorized'}), 401
    session['user_id'] = user.id
    return jsonify({'id': user.user_num, 'username': user.username})


@app.route('/logout', methods=['POST'])
@login_required
def logout_user():
    session.pop('user_id', None)
    return jsonify({'message': 'Logged out'})


@app.route('/change-password', methods=['POST'])
@login_required
def change_password():
    user_id = session.get('user_id')
    if not user_id:
        return jsonify({'message': 'Unauthorized'}), 401
    user = User.query.filter_by(id=user_id).first()
    old_password = request.json['old_password']
    new_password = request.json['new_password']
    cf_password = request.json['cf_password']
    if not bcrypt.check_password_hash(user.password, old_password) or new_password != cf_password or old_password == new_password:
        return jsonify({'message': 'Unauthorized'}), 401
    user.password = bcrypt.generate_password_hash(new_password)
    db.session.commit()
    return jsonify({'id': user.user_num, 'username': user.username})

@app.route('/slidebar/<id>')
@login_required
def identiy(id):
    user = dataset[int(id) - 1]
    name = user['first_name'] + ' ' + user['last_name']
    return jsonify({'name': name})

@app.route('/individual/<id>/level')
@login_required
def one_level(id):
    user = dataset[int(id) - 1]
    level = user['level']
    point = user['total_points']
    general = ['Fresher: 0-33', 'Junior: 34-67', 'Senior: 68-100', 'Expert: 100-']
    return jsonify({"point": point, "level": level, 'general': general})


@app.route('/individual/<id>/metrics')
@login_required
def one_metrics(id):
    user = dataset[int(id) - 1]
    point = user['total_points']
    rank = user['rank']
    total = len(dataset)
    completion_rate = user['completion_rate']
    average_peer_rating = user['average_peer_rating']
    return jsonify({'point': point, 'rank': rank, 'total': total, 'completion_rate': completion_rate, 'average_peer_rating': average_peer_rating})


@app.route('/individual/<id>/monthly_pts')
@login_required
def one_monthly_pts(id):
    user = dataset[int(id) - 1]
    monthly_points = dict()
    for timeline in user['performance']:
        monthly_points[timeline] = user['performance'][timeline]['total_points']
    return jsonify(monthly_points)


@app.route('/individual/<id>/sprint_pts')
@login_required
def one_sprint_pts(id):
    user = dataset[int(id) - 1]
    sprint_points = dict()
    for sprint in user['sprint']:
        sprint_points[sprint] = dict()
        sprint_points[sprint]['completed'] = user['sprint'][sprint]['completed']
        sprint_points[sprint]['incomplete'] = user['sprint'][sprint]['incomplete']
    return jsonify(sprint_points)


@app.route('/individual/<id>/monthly_tasks')
@login_required
def one_monthly_tasks(id):
    user = dataset[int(id) - 1]
    monthly_tasks = dict()
    for timeline in user['performance']:
        monthly_tasks[timeline] = dict()
        monthly_tasks[timeline]['1pt_task'] = user['performance'][timeline]['1pt_task']
        monthly_tasks[timeline]['2pt_task'] = user['performance'][timeline]['2pt_task']
        monthly_tasks[timeline]['3pt_task'] = user['performance'][timeline]['3pt_task']
    return jsonify(monthly_tasks)


@app.route('/individual/<id>/monthly_time')
@login_required
def one_month_time(id):
    user = dataset[int(id) - 1]
    monthly_time = dict()
    for timeline in user['performance']:
        monthly_time[timeline] = dict()
        monthly_time[timeline]['1pt_time'] = user['performance'][timeline]['1pt_time']
        monthly_time[timeline]['2pt_time'] = user['performance'][timeline]['2pt_time']
        monthly_time[timeline]['3pt_time'] = user['performance'][timeline]['3pt_time']
    return jsonify(monthly_time)


@app.route('/team/monthly_rank/<month>')
@login_required
def whole_monthly_rank(month):
    whole_ranking = whole_rank(month)
    return jsonify(whole_ranking)


@app.route('/team/<id>/monthly_rank/<month>')
@login_required
def team_monthly_rank(id, month):
    user = dataset[int(id) - 1]
    team = user['team']
    return jsonify(team_ranking(month)[team][:5])


@app.route('/team/completion')
@login_required
def team_completion():
    return jsonify(team_rate)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)