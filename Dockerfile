FROM python:3.9-alpine

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

ARG API_KEY

ENV WEBSITE "https://my.api.mockaroo.com"
ENV API_KEY $API_KEY

COPY . .

EXPOSE 5000

CMD [ "python", "app.py" ]